#!/bin/bash
#!
band=B2
fov=0.05

export PYTHONPATH=.:$PYTHONPATH
for time_range in 0.2 4
do
  for dec in -80 -45 0 15
  do
    python polarisation.py --rmax 1e4 --npixel 512 --band ${band} --pbtype MID_FEKO_${band} --fov ${fov} \
    --show False --declination ${dec}  --integration_time 600 --npatch 15 --use_natural True \
    --use_agg True --time_chunk 1800 --time_range -${time_range} ${time_range}  \
    --name polarisation_simulation_${band}_dec${dec}_time${time_range} | tee -a \
    polarisation_simulation_${band}_dec${dec}_time${time_range}.log
  done
done
