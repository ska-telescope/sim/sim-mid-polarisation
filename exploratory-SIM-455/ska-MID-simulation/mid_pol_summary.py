"""Simulation of the effect of errors on MID observations

"""
import logging
import sys

import numpy

from tabulate import tabulate
from rascil.data_models import import_skycomponent_from_hdf5
from rascil.processing_components import import_image_from_fits, \
    create_vp, calculate_blockvisibility_parallactic_angles, \
    convert_azelvp_to_radec, find_skycomponents, find_skycomponent_matches


def init_logging():
    logging.basicConfig(filename='mid_pol_summary.log',
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)


init_logging()
log = logging.getLogger("logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def find_vp_actual(telescope="MID_FEKO_B2", normalise=True, test_vp=False):
    vp = create_vp(telescope=telescope)
    if test_vp:
        vp.data[:, 0, ...] = 1.0
        vp.data[:, 1, ...] = 0.0
        vp.data[:, 2, ...] = 0.0
        vp.data[:, 3, ...] = 1.0
    if normalise:
        g = numpy.zeros([4])
        g[0] = numpy.max(numpy.abs(vp.data[:, 0, ...]))
        g[3] = numpy.max(numpy.abs(vp.data[:, 3, ...]))
        g[1] = g[2] = numpy.sqrt(g[0] * g[3])
        for chan in range(4):
            vp.data[:, chan, ...] /= g[chan]
    return vp


def find_vp_renormalised(telescope="MID_FEKO_B2", flatten=True, minpb=0.1):
    vp = create_vp(telescope=telescope)
    g = numpy.zeros([4])
    g[0] = numpy.max(numpy.abs(vp.data[:, 0, ...]))
    g[3] = numpy.max(numpy.abs(vp.data[:, 3, ...]))
    g[1] = g[2] = numpy.sqrt(g[0] * g[3])
    for pol in range(4):
        vp.data[:, pol, ...] /= g[pol]
    if flatten:
        norm = numpy.sqrt(0.5 * (numpy.abs(g[0].data[...]) ** 2 + numpy.abs(g[-1].data[...]) ** 2))
        vp.data[norm > minpb] /= norm[norm > minpb]
        vp.data[norm <= minpb] = 0.0
    return vp


def make_ejterm_rotated(model, bvis):
    vp = find_vp_actual(telescope="MID_FEKO_B2")
    pa = numpy.renormalised(calculate_blockvisibility_parallactic_angles(bvis))
    vp_rotated = convert_azelvp_to_radec(vp, model, pa)
    return vp_rotated

def do_summary(args):
    
    print("\nStarting MID polarisation: summary\n")
    
    import pprint
    pp = pprint.PrettyPrinter()
    pp.pprint(vars(args))
    
    model = import_image_from_fits(args.model)
    
    table = []
    headers = ["Polarisation", "max (mJy)", "min (mJy)", "RMS (mJy)", "med abs dev median (mJy)"]
    for pol in range(model.npol):
        pmax = numpy.max(model.data[:,pol,...]) * 1e3
        pmin = numpy.min(model.data[:,pol,...]) * 1e3
        prms = numpy.std(model.data[:,pol,...]) * 1e3
        pmadm = numpy.median(numpy.abs(model.data[:,pol,...] - numpy.median(model.data[:,pol,...]))) * 1e3
        polname = model.polarisation_frame.names[pol]
        table.append([polname, "{0:.6f}".format(pmax), "{0:.6f}".format(pmin), "{0:.6f}".format(prms),
                      "{0:.6f}".format(pmadm)])

    print("Image statistics:\n" + tabulate(table, headers=headers))

    components = find_skycomponents(model, threshold=args.flux_limit)
    print("\nFound {} components".format(len(components)))
    table = []
    headers = ["Name", "Separation (deg)", "Stokes I (mJy)", "Pol flux (mJy)", "Fractional pol", "P.A. (deg)",
               "Stokes V (mJy)", "Fractional V pol"]
    components = sorted(components, key=lambda comp: model.phasecentre.separation(comp.direction).deg, reverse=False)
    rs = []
    ipols = []
    ppols = []
    fpols = []
    papols = []
    vpols = []
    fvpols = []
    for comp in components:
        r = model.phasecentre.separation(comp.direction).deg
        ipol = comp.flux[0][0] * 1e3
        vpol = abs(comp.flux[0][3] * 1e3)
        ppol = numpy.sqrt(comp.flux[0][1] ** 2 + comp.flux[0][2] ** 2) * 1e3
        papol = 180.0 * numpy.arctan2(comp.flux[0][1], comp.flux[0][2]) / numpy.pi
        fpol = ppol / ipol
        fvpol = vpol / ipol
        table.append([comp.name, "{0:.4f}".format(r),  "{0:.4f}".format(ipol), "{0:.4f}".format(ppol),
                      "{0:.4f}".format(fpol),  "{0:.1f}".format(papol), "{0:.4f}".format(vpol),
                      "{0:.4f}".format(fvpol)])
        rs.append(r)
        ipols.append(ipol)
        ppols.append(ppol)
        fpols.append(fpol)
        papols.append(papol)
        vpols.append(vpol)
        fvpols.append(fvpol)

    rs = numpy.array(rs)
    ipols = numpy.array(ipols)
    ppols = numpy.array(ppols)
    fpols = numpy.array(fpols)
    papols = numpy.array(papols)
    vpols = numpy.array(vpols)
    fvpols = numpy.array(fvpols)

    print("Component statistics:\n" + tabulate(table, headers=headers))
    
    import matplotlib.pyplot as plt
    values = [(ipols, "Stokes I", True),
              (ppols, "P", True),
              (papols, "Position angle", False)]
    for value in values:
        plt.clf()
        plt.title(args.model)
        plt.xlabel("Distance from pointing centre (deg)")
        plt.ylabel(value[1])
        if value[2]:
            plt.semilogy(rs, value[0], '.')
        else:
            plt.plot(rs, value[0], '.')
        plotfile = args.model.replace(".fits", "_{}.png".format(value[1]))
        plt.savefig(plotfile)
        plt.show(block=False)

    values = [(fpols, "Fractional polarisation", False),
              (fvpols, "Fractional V", False)]
    for value in values:
        plt.clf()
        plt.title(args.model)
        plt.xlabel("Distance from pointing centre (deg)")
        plt.ylabel(value[1])
        if value[2]:
            plt.semilogy(rs, value[0], '.')
        else:
            plt.plot(rs, value[0], '.')
        a = numpy.sum(rs**2 * value[0]) / numpy.sum(rs**4)
        plt.plot(rs, a*rs**2, color='red', label='{0} = {1:.3f} * radius^2'.format(value[1], a))

        plotfile = args.model.replace(".fits", "_{}.png".format(value[1]))
        plt.legend()
        plt.savefig(plotfile)
        plt.show(block=False)

    return True


def get_args():
    # Get command line inputs
    import argparse
    
    parser = argparse.ArgumentParser(description='Summmarize polarisation error in MID observations')
    parser.add_argument('--model', type=str, default="mid_simulation_polarisation_subtracted_restored.fits",
                        help='Model to be read')
    parser.add_argument('--flux_limit', type=float, default=0.01, help='Flux limit (Jy)')
    
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_args()
    
    do_summary(args)
