#!/bin/bash -e
mkdir -p 1mJy_fft_rapid
cd 1mJy_fft_rapid
python3 ../mid_pol_sim.py --flux_limit 0.001  --time_chunk 360
python3 ../mid_pol_image.py --mode cip --cellsize 2.5e-5 --threshold 0.0001 --niter 10000 --nmajor 8 \
--number_ms 80 --npixel 1536  --serial True
python3 ../mid_pol_summary.py --model mid_simulation_polarisation_deconvolved.fits --flux_limit 0.003
python3 ../mid_pol_subtract_model.py --flux_limit 0.001 --number_ms 80 \
  --model mid_simulation_polarisation_restored.fits --model_only False --use_dft False
python3 ../mid_pol_image.py --msname mid_simulation_polarisation_subtracted.ms --cellsize 2.5e-5 --npixel 1536 \
  --threshold 0.0001 --niter 10000 --nmajor 8 --number_ms 80 --serial True
python3 ../mid_pol_summary.py --model mid_simulation_polarisation_subtracted_restored.fits --flux_limit 0.003

