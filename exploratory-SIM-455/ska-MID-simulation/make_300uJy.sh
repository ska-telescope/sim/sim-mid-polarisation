#!/bin/bash -e
mkdir -p 300uJy
cd 300uJy
python3 ../mid_pol_sim.py --flux_limit 0.0003 --time_chunk 3600
python3 ../mid_pol_image.py --mode cip --cellsize 3e-5 --threshold 0.0001 --nmajor 8 --number_ms 8
python3 ../mid_pol_summary.py --model mid_simulation_polarisation_restored.fits --flux_limit 0.003
python3 ../mid_pol_subtract_model.py --flux_limit 0.001 --number_ms 8  \
  --model mid_simulation_polarisation_restored.fits  --model_only False --use_dft True
python3 ../mid_pol_image.py --msname mid_simulation_polarisation_subtracted.ms --cellsize 3e-5 \
  --threshold 0.0001 --nmajor 8 --number_ms 8
python3 ../mid_pol_summary.py --model mid_simulation_polarisation_subtracted_restored.fits --flux_limit 0.003

