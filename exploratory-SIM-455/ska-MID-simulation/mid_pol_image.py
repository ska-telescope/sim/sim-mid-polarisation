"""Simulation of the effect of errors on MID observations

Imaging step
"""
import logging
import sys

import numpy

from astropy.time import Time

from rascil.data_models.polarisation import PolarisationFrame

from rascil.processing_components import create_blockvisibility_from_ms, create_image_from_visibility, \
    export_image_to_fits, qa_image, create_vp

from rascil.workflows.rsexecute import invert_list_rsexecute_workflow, sum_invert_results_rsexecute, \
    weight_list_rsexecute_workflow, continuum_imaging_list_rsexecute_workflow

from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute

import pprint
pp = pprint.PrettyPrinter()

def init_logging():
    logging.basicConfig(filename='mid_pol_image.log',
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)


init_logging()
log = logging.getLogger("logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def find_vp_actual(telescope="MID_FEKO_B2", normalise=True, test_vp=False):
    vp = create_vp(telescope=telescope)
    if test_vp:
        vp.data[:, 0, ...] = 1.0
        vp.data[:, 1, ...] = 0.0
        vp.data[:, 2, ...] = 0.0
        vp.data[:, 3, ...] = 1.0
    if normalise:
        g = numpy.zeros([4])
        g[0] = numpy.max(numpy.abs(vp.data[:, 0, ...]))
        g[3] = numpy.max(numpy.abs(vp.data[:, 3, ...]))
        g[1] = g[2] = numpy.sqrt(g[0] * g[3])
        for chan in range(4):
            vp.data[:, chan, ...] /= g[chan]
    return vp


def do_image(args):

    print("\nStarting MID polarisation: imaging\n")

    pp.pprint(vars(args))
    
    rsexecute.set_client(use_dask=(args.serial != "True"), n_workers=args.nworkers, threads_per_worker=args.nthreads)
    rsexecute.run(init_logging)
    rsexecute.init_statistics()
    
    def load_ms(ibvis):
        msname = args.msname.replace('.ms', '_{}.ms'.format(ibvis))
        bvis = create_blockvisibility_from_ms(msname)[0]
        times = Time(bvis.time / 86400.0, format='mjd', scale='utc')
        log.info("Read blockVis {}, time range  {} to {}".format(ibvis, times[0].isot, times[-1].isot))
        return bvis

    bvis_list = [rsexecute.execute(load_ms)(ibvis) for ibvis in range(args.number_ms)]
    bvis_list = rsexecute.persist(bvis_list)

    cellsize = args.cellsize
    npixel = args.npixel
    model_list = [rsexecute.execute(create_image_from_visibility)(bvis, npixel=npixel, cellsize=cellsize,
                                                                  polarisation_frame=PolarisationFrame("stokesIQUV"))
                  for bvis in bvis_list]
    model_list = rsexecute.persist(model_list)

    bvis_list = weight_list_rsexecute_workflow(bvis_list, model_list, weighting=args.weighting,
                                               robustness=args.robustness)
    
    if args.mode == "invert":
    
        psf_list = invert_list_rsexecute_workflow(bvis_list, template_model_imagelist=model_list, dopsf=True,
                                                  context='ng')
        psf_list = sum_invert_results_rsexecute(psf_list)
        result = rsexecute.compute(psf_list, sync=True)
        psf, sumwt = result
    
        print(qa_image(psf))
        export_image_to_fits(psf, args.msname.replace(".ms", "_psf.fits"))

        dirty_list = invert_list_rsexecute_workflow(bvis_list, template_model_imagelist=model_list, context='ng')
        dirty_list = sum_invert_results_rsexecute(dirty_list)
        result = rsexecute.compute(dirty_list, sync=True)
        dirty, sumwt = result
    
        print(qa_image(dirty))
        export_image_to_fits(dirty, args.msname.replace(".ms", "_dirty.fits"))
    elif args.mode == "cip":
        continuum_imaging_list = continuum_imaging_list_rsexecute_workflow(bvis_list,
                                                                           model_imagelist=model_list,
                                                                           context='ng', vis_slices=1,
                                                                           niter=args.niter,
                                                                           algorithm='mmclean',
                                                                           fractional_threshold=0.2,
                                                                           nmoment=1,
                                                                           scales=[0],
                                                                           threshold=args.threshold,
                                                                           nmajor=args.nmajor,
                                                                           gain=0.1,
                                                                           deconvolve_facets=1,
                                                                           deconvolve_overlap=0,
                                                                           deconvolve_taper='tukey',
                                                                           psf_support=1024)
        result = rsexecute.compute(continuum_imaging_list, sync=True)

        centre=0
        deconvolved = result[0][centre]
        residual = result[1][centre]
        restored = result[2][centre]

        print(qa_image(deconvolved, context='Clean image'))
        export_image_to_fits(deconvolved, args.msname.replace(".ms", "_deconvolved.fits"))

        print(qa_image(restored, context='Restored clean image'))
        export_image_to_fits(restored, args.msname.replace(".ms", "_restored.fits"))

        print(qa_image(residual[0], context='Residual clean image'))
        export_image_to_fits(residual[0], args.msname.replace(".ms", "_residual.fits"))

    rsexecute.save_statistics("mid_pol_image")
    rsexecute.close()
    
    return True


def get_args():
    # Get command line inputs
    import argparse
    
    parser = argparse.ArgumentParser(description='SKA MID imaging using RASCIL')
    parser.add_argument('--context', type=str, default='2d', help="Imaging context '2d' | 'ng'")
    parser.add_argument('--mode', type=str, default='cip', help="Imaging mode 'invert'|'cip'|'ical'")
    parser.add_argument('--msname', type=str, default="mid_simulation_polarisation.ms",
                        help='MeasurementSet to be read')
    parser.add_argument('--number_ms', type=int, default=8,
                        help='Number of MeasurementSets to be read')

    parser.add_argument('--model_image', type=str, default=None, help='Initial model image')

    parser.add_argument('--npixel', type=int, default=1536, help='Number of pixels')
    parser.add_argument('--cellsize', type=float, default=4e-5, help='Cellsize in radians')

    parser.add_argument('--wstep', type=float, default=None, help='Step in w (wavelengths)')
    parser.add_argument('--nwplanes', type=int, default=None, help='Number of w planes')
    parser.add_argument('--nwslabs', type=int, default=None, help='Number of w slabs')
    parser.add_argument('--amplitude_loss', type=float, default=0.02, help='Amplitude loss due to w sampling')
    parser.add_argument('--oversampling', type=int, default=16, help='Oversampling in w projection kernel')

    parser.add_argument('--epsilon', type=float, default=1e-12, help='Accuracy in nifty gridder')
    parser.add_argument('--weighting', type=str, default='robust', help='Type of weighting')
    parser.add_argument('--robustness', type=float, default=0.0, help='Robustness for robust weighting')

    parser.add_argument('--nmoment', type=int, default=1, help='Number of spectral moments in MSMFS Clean')
    parser.add_argument('--nmajor', type=int, default=5, help='Number of major cycles')
    parser.add_argument('--niter', type=int, default=1000, help='Number of iterations per major cycle')
    parser.add_argument('--fractional_threshold', type=float, default=0.1,
                        help='Fractional threshold to terminate major cycle')
    parser.add_argument('--threshold', type=float, default=0.01, help='Absolute threshold (Jy) to terminate')
    parser.add_argument('--window_shape', type=str, default=None, help="Window shape 'quarter'|'no_edge")
    parser.add_argument('--window_edge', type=int, default=None, help='Window edge (pixels)')
    parser.add_argument('--deconvolve_facets', type=int, default=1, help='Number of facets in deconvolution')
    parser.add_argument('--deconvolve_overlap', type=int, default=128, help='Overlap in deconvolution')
    parser.add_argument('--deconvolve_taper', type=str, default='tukey', help="Overlap function 'linear'|'tukey'")
    parser.add_argument('--restore_facets', type=int, default=1, help='Number of facets in restore')

    parser.add_argument('--nthreads', type=int, default=4, help='Number of threads')
    parser.add_argument('--memory', type=int, default=8, help='Memory per worker (GB)')
    parser.add_argument('--nworkers', type=int, default=4, help='Number of workers')
    parser.add_argument('--serial', type=str, default='False', help='Use serial processing?')

    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = get_args()
    do_image(args)
