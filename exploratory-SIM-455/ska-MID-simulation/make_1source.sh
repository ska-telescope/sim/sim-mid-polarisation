#!/bin/bash -e
mkdir -p 1source
cd 1source
python3 ../mid_pol_sim.py --flux_limit 0.5 --time_chunk 1800 --time_range -4 4
python3 ../mid_pol_image.py --mode cip --cellsize 3e-5 --threshold 0.001 --nmajor 5 --number_ms 16
python3 ../mid_pol_subtract_model.py --flux_limit 0.5 --number_ms 16 \
  --model mid_simulation_polarisation_restored.fits \
  --model_only False --use_dft True --number_ms 16
python3 ../mid_pol_image.py --mode cip --cellsize 3e-5 --threshold 0.001 --nmajor 5 --number_ms 16 \
  --msname mid_simulation_polarisation_subtracted.ms
