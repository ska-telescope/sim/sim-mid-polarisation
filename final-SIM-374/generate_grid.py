
import numpy
from astropy.coordinates import SkyCoord
import astropy.units as u

def generate_grid(direction, side_length, fov):
    """ Fine ra, dec coordinates to form a grid in the tangent plane
    
    :param direction:
    :param side_length:
    :param fov:
    :return:
    """
    l_max = numpy.sin(0.5 * fov)
    sin_dec0 = numpy.sin(direction.dec.rad)
    cos_dec0 = numpy.cos(direction.dec.rad)
    
    coords = list()
    for l in numpy.linspace(-l_max, l_max, side_length):
        for m in numpy.linspace(-l_max, l_max, side_length):
            # Convert l, m to ra, dec
            n = numpy.sqrt(1 - l**2 - m**2)
            dec = numpy.arcsin(n * sin_dec0 + m * cos_dec0)
            ra = direction.ra.rad + numpy.arctan2(l, cos_dec0 * n - m * sin_dec0)
            coords.append(SkyCoord(ra=ra * u.rad, dec=dec * u.rad))
    
    return coords

if __name__ == "__main__":
    
    centre = SkyCoord(ra="180deg", dec="-45deg")
    print(generate_grid(centre, 5, 0.05))
