#!/usr/bin/env python3

"""Initial script to run polarisation simulations."""

import glob
import json
import logging
import sys

from astropy.time import Time, TimeDelta
import matplotlib
matplotlib.use('Agg')
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt
import numpy


def make_plots(prefix, results):
    centre = len(results) // 2
    ra0 = float(results[centre]["ra"])
    dec0 = float(results[centre]["dec"])
    
    """Plot selected results."""
    num_pos = len(results)
    ra = numpy.zeros([num_pos])
    dec = numpy.zeros([num_pos])
    stokes_i = numpy.zeros([num_pos])
    stokes_q = numpy.zeros([num_pos])
    stokes_u = numpy.zeros([num_pos])
    stokes_v = numpy.zeros([num_pos])
    
    # Loop over source positions.
    for iresult, result in enumerate(results):
        ra[iresult] = float(result["ra"])
        dec[iresult] = float(result["dec"])
        stokes_i[iresult] = result['onsource_I']
        stokes_q[iresult] = result['onsource_Q']
        stokes_u[iresult] = result['onsource_U']
        stokes_v[iresult] = result['onsource_V']
    
    # Convert to direction cosines for plotting.
    x = numpy.cos(dec) * numpy.sin(ra - ra0)
    y = numpy.cos(numpy.radians(dec0)) * numpy.sin(dec) - \
        numpy.sin(numpy.radians(dec0)) * numpy.cos(dec) * numpy.cos(ra - ra0)
    
    xrange = 1.5 * (numpy.max(x) - numpy.min(x))
    yrange = 1.5 * (numpy.max(y) - numpy.min(y))
    xcentre = numpy.average(x)
    ycentre = numpy.average(y)
    
    # Sub-plots, one for each Stokes parameter.
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(221)
    sc = ax.scatter(x, y, c=stokes_i, s=30, cmap='plasma')
    plt.colorbar(sc)
    plt.gca().set_title('Measured Stokes I')
    plt.gca().set_xlim([xcentre - xrange/2.0, xcentre + xrange/2.0])
    plt.gca().set_ylim([ycentre - yrange/2.0, ycentre + yrange/2.0])
    #plt.gca().axis('equal')
    ax = fig.add_subplot(222)
    sc = ax.scatter(x, y, c=stokes_q, s=30, cmap='plasma')
    plt.colorbar(sc)
    plt.gca().set_title('Measured Stokes Q')
    plt.gca().set_xlim([xcentre - xrange/2.0, xcentre + xrange/2.0])
    plt.gca().set_ylim([ycentre - yrange/2.0, ycentre + yrange/2.0])
    #plt.gca().axis('equal')
    ax = fig.add_subplot(223)
    sc = ax.scatter(x, y, c=stokes_u, s=30, cmap='plasma')
    plt.colorbar(sc)
    plt.gca().set_title('Measured Stokes U')
    plt.gca().set_xlim([xcentre - xrange/2.0, xcentre + xrange/2.0])
    plt.gca().set_ylim([ycentre - yrange/2.0, ycentre + yrange/2.0])
    #plt.gca().axis('equal')
    ax = fig.add_subplot(224)
    sc = ax.scatter(x, y, c=stokes_v, s=30, cmap='plasma')
    plt.colorbar(sc)
    plt.gca().set_title('Measured Stokes V')
    plt.gca().set_xlim([xcentre - xrange/2.0, xcentre + xrange/2.0])
    plt.gca().set_ylim([ycentre - yrange/2.0, ycentre + yrange/2.0])
    #plt.gca().axis('equal')
    
    # Common axis labels.
    ax = fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax.set_xlabel('x direction cosine\n\n{}'.format(prefix), labelpad=20)
    ax.set_ylabel('y direction cosine', labelpad=40)
    # Hide ticks for this new axis.
    ax.set_xticks([])
    ax.set_yticks([])
    
    # Save and close.
    fig.tight_layout()
    plt.savefig('%s.png' % (prefix))
    plt.close('all')

if __name__ == '__main__':
    
    import glob
    filenames = glob.glob("*.csv")
    for filename in filenames:
        
        try:
            import csv
            results = list()
            with open(filename, 'r') as csvfile:
                print(filename)
                reader = csv.DictReader(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for row in reader:
                    results.append(row)
                csvfile.close()
    
            prefix = filename.replace(".csv", "")
            make_plots(prefix, results)
        except IndexError:
            pass
