"""Simulation of the effect of errors on MID observations

"""
import logging
import sys

import numpy
from astropy.time import Time
from astropy.coordinates import SkyCoord
import astropy.units as u

from rascil.data_models import PolarisationFrame, convert_linear_to_stokes, convert_stokes_to_linear, \
    import_skycomponent_from_hdf5, Skycomponent
from rascil.processing_components import create_blockvisibility_from_ms, import_image_from_fits, \
    create_vp, export_blockvisibility_to_ms, calculate_blockvisibility_parallactic_angles, \
    convert_azelvp_to_radec, find_skycomponents, simulate_gaintable_from_voltage_pattern, \
    apply_voltage_pattern_to_skycomponent, copy_visibility, create_simulation_components, \
    find_pb_width_null, export_image_to_fits
from rascil.workflows.rsexecute import predict_fft_image_rsexecute_workflow, subtract_list_rsexecute_workflow, \
    zero_list_rsexecute_workflow, predict_dft_rsexecute_workflow
from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute

def init_logging():
    logging.basicConfig(filename='mid_pol_subtract_model.log',
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)

init_logging()
log = logging.getLogger("logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def find_vp_actual(telescope="MID_FEKO_B2", normalise=True, test_vp=False):
    vp = create_vp(telescope=telescope)
    if test_vp:
        vp.data[:, 0, ...] = 1.0
        vp.data[:, 1, ...] = 0.0
        vp.data[:, 2, ...] = 0.0
        vp.data[:, 3, ...] = 1.0
    if normalise:
        g = numpy.zeros([4])
        g[0] = numpy.max(numpy.abs(vp.data[:, 0, ...]))
        g[3] = numpy.max(numpy.abs(vp.data[:, 3, ...]))
        g[1] = g[2] = numpy.sqrt(g[0] * g[3])
        for chan in range(4):
            vp.data[:, chan, ...] /= g[chan]
    return vp

def find_vp_renormalised(telescope="MID_FEKO_B2", flatten=True, minpb=0.1):
    vp = create_vp(telescope=telescope)
    g = numpy.zeros([4])
    g[0] = numpy.max(numpy.abs(vp.data[:, 0, ...]))
    g[3] = numpy.max(numpy.abs(vp.data[:, 3, ...]))
    g[1] = g[2] = numpy.sqrt(g[0] * g[3])
    for pol in range(4):
        vp.data[:, pol, ...] /= g[pol]
    if flatten:
        norm = numpy.sqrt(0.5*(numpy.abs(vp.data[:, 0, ...])**2 + numpy.abs(vp.data[:, 3, ...])**2))
        for pol in range(4):
            vp.data[:, pol, ...][norm>minpb] /= norm[norm>minpb]
            vp.data[:, pol, ...][norm<=minpb] = 0.0
    return vp

def make_ejterm_rotated(model, bvis):
    vp = find_vp_actual(telescope="MID_FEKO_B2")
    pa = numpy.average(calculate_blockvisibility_parallactic_angles(bvis))
    vp_rotated = convert_azelvp_to_radec(vp, model, pa)
    return vp_rotated


def do_subtraction(args):
    
    print("\nStarting MID polarisation: subtraction of model\n")
    
    import pprint
    pp = pprint.PrettyPrinter()
    pp.pprint(vars(args))
    
    rsexecute.set_client(use_dask=True, n_workers=args.nworkers, threads_per_worker=1)
    rsexecute.run(init_logging)
    rsexecute.init_statistics()
    
    def load_ms(ibvis):
        msname = args.msname.replace('.ms', '_{}.ms'.format(ibvis))
        bvis = create_blockvisibility_from_ms(msname)[0]
        times = Time(bvis.time / 86400.0, format='mjd', scale='utc')
        log.info("Read blockVis {}, time range  {} to {}".format(ibvis, times[0].isot, times[-1].isot))
        print("Read: {} {}".format(ibvis, numpy.max(numpy.abs(bvis.flagged_vis[:,1:,0,...]))))
        return bvis

    bvis_list = [rsexecute.execute(load_ms)(ibvis) for ibvis in range(args.number_ms)]
    bvis_list = rsexecute.persist(bvis_list)

    vp = find_vp_renormalised()
    export_image_to_fits(vp, "vp_flatten.fits")

    vp = find_vp_renormalised(flatten=False)
    export_image_to_fits(vp, "vp.fits")

    if args.use_dft == "True":
        
        model = import_image_from_fits(args.model)
        components_stokesI = find_skycomponents(model, threshold=args.flux_limit)
        print("Found {} components".format(len(components_stokesI)))

        for comp in components_stokesI: comp.flux[0][1:4] = 0.0

        # Make one voltage pattern for each blockvis and persist on Dask
        vp_list = [rsexecute.execute(find_vp_renormalised)() for ibvis, _ in enumerate(bvis_list)]
        vp_list = rsexecute.persist(vp_list)
    
        # Simulate a gaintable for each component, for each blockvis
        gt_list = [rsexecute.execute(simulate_gaintable_from_voltage_pattern, nout=len(components_stokesI))
                   (bvis, components_stokesI, vp_list[ibv], use_radec=False)
                   for ibv, bvis in enumerate(bvis_list)]
    
        # Now predict the visibility for each blockvis
        model_bvis_list = predict_dft_rsexecute_workflow(bvis_list, components_stokesI, gt_list=gt_list)

    else:
        def zero_quv(model):
            assert model.polarisation_frame == PolarisationFrame("stokesIQUV")
            model.data[:, 1:4, :, :] = 0.0
            return model
        
        model_list = [rsexecute.execute(import_image_from_fits)(args.model) for i in range(args.number_ms)]
        model_list = [rsexecute.execute(zero_quv)(m) for m in model_list]
        model_list = rsexecute.persist(model_list)
        
        # Make one voltage pattern for each blockvis and persist on Dask
        vp_list = [rsexecute.execute(make_ejterm_rotated)(model_list[ibvis], bvis) for ibvis, bvis in enumerate(bvis_list)]
        vp_list = rsexecute.persist(vp_list)
        
        model_bvis_list = predict_fft_image_rsexecute_workflow(bvis_list, model_list, vp_list=vp_list, context='ng')

    # Now we don't want to subtract the stokes I part of the blockvis so we
    # zero out the stokes I part
    def zero_i(bvis):
        ivis = convert_linear_to_stokes(bvis.vis, polaxis=-1)
        ivis[..., 0] = 0.0
        ivis = convert_stokes_to_linear(ivis, -1)
        bvis.data['vis'] = ivis
        return bvis

    model_bvis_list = [rsexecute.execute(zero_i)(bvis) for bvis in model_bvis_list]
    model_bvis_list = rsexecute.persist(model_bvis_list)

    if args.model_only == "True":
        log.info("Writing only the model to the MeasurementSets")
        bvis_list = [rsexecute.execute(copy_visibility)(mv) for mv in model_bvis_list]
    else:
        log.info("Writing (observed - model) to the MeasurementSets")
        bvis_list = subtract_list_rsexecute_workflow(bvis_list, model_bvis_list)
    
    bvis_list = rsexecute.persist(bvis_list)
    
    def save_ms(ibvis, bvis):
        times = Time(bvis.time / 86400.0, format='mjd', scale='utc')
        log.info("Writing blockVis {}, time range  {} to {}".format(ibvis, times[0].isot, times[-1].isot))
        msname = args.output_msname.replace('.ms', '_{}.ms'.format(ibvis))
        export_blockvisibility_to_ms(msname, [bvis])
        print("Written: {} {}".format(ibvis, numpy.max(numpy.abs(bvis.flagged_vis[:,1:,0,...]))))
        return bvis
    
    bvis_list = [rsexecute.execute(save_ms)(ibvis, bvis) for ibvis, bvis in enumerate(bvis_list)]
    bvis_list = rsexecute.compute(bvis_list, sync=True)
    
    assert len(bvis_list) > 0
    
    rsexecute.save_statistics('mid_pol_subtract_model')
    
    rsexecute.close()
    
    from rascil.processing_components import plot_pa
    import matplotlib.pyplot as plt
    plt.clf()
    plot_pa(bvis_list)
    plt.show(block=False)
    
    return True


def get_args():
    # Get command line inputs
    import argparse
    
    parser = argparse.ArgumentParser(description='Correct polarisation error in MID observations')
    parser.add_argument('--ra', type=float, default=+15.0, help='Right ascension (degrees)')
    parser.add_argument('--declination', type=float, default=-45.0, help='Declination (degrees)')
    parser.add_argument('--msname', type=str, default="mid_simulation_polarisation.ms",
                        help='MeasurementSet to be read')
    parser.add_argument('--number_ms', type=int, default=8,
                        help='Number of MeasurementSets to be read')
    parser.add_argument('--output_msname', type=str, default="mid_simulation_polarisation_subtracted.ms",
                        help='MeasurementSet to be written')
    parser.add_argument('--model', type=str, default="mid_simulation_polarisation_deconvolved.fits",
                        help='Model to be read')
    parser.add_argument('--use_dft', type=str, default="True", help="Use DFT of components?")
    parser.add_argument('--model_only', type=str, default="False",
                        help='Write only the model visibility?')
    parser.add_argument('--flux_limit', type=float, default=0.03, help='Flux limit for source finding (Jy)')
    parser.add_argument('--pbradius', type=float, default=1.5, help='Radius of sources to include (in HWHM)')

    parser.add_argument('--nnodes', type=int, default=1, help='Number of nodes')
    parser.add_argument('--nthreads', type=int, default=4, help='Number of threads')
    parser.add_argument('--memory', type=int, default=8, help='Memory per worker (GB)')
    parser.add_argument('--nworkers', type=int, default=4, help='Number of workers')
    parser.add_argument('--serial', type=str, default='False', help='Use serial processing?')
    
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = get_args()
    
    do_subtraction(args)
