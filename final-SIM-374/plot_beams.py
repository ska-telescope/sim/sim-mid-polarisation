
import matplotlib.pyplot as plt

from rascil.processing_components import create_vp

def plot_beam(vp, band, value):
    plt.clf()
    # fig, ax = plt.subplots(2,2, projection=vp.wcs.sub([1, 2]))

    fig = plt.figure()

    pol = 0
    pols = ["X->X", "X->Y", "Y->X", "Y->Y"]
    for row in range(2):
        for col in range(2):
            ax = fig.add_subplot(2, 2, pol+1, projection=vp.wcs.sub([1, 2]))
            cm = ax.imshow(vp.data[0, pol], origin='lower', cmap='plasma')
            ax.set_title(pols[pol])
            plt.colorbar(cm, ax=ax, orientation='vertical', shrink=0.7)
            pol += 1
            
    ax = fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_title("{} {}".format(band, value))

    #plt.tight_layout()
    plt.savefig('MID_{}_{}_beam.png'.format(band, value))
    plt.show(block=False)

if __name__ == '__main__':
    
    for band in ['B1', 'B2', 'Ku']:
        vp = create_vp(telescope='MID_FEKO_{}'.format(band))
        vp.data = vp.data.real
        plot_beam(vp, band, 'real')
        vp = create_vp(telescope='MID_FEKO_{}'.format(band))
        vp.data = vp.data.imag
        plot_beam(vp, band, 'imag')
