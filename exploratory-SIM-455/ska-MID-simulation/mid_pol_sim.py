"""Simulation of the effect of errors on MID observations

This measures the change in a dirty imagethe induced by various errors:
- The sky can be a point source at the half power point or a realistic sky constructed from S3-SEX catalog.
- The observation is by MID over a range of hour angles
- Processing can be divided into chunks of time (default 1800s)
- Dask is used to distribute the processing over a number of workers.
- Various plots are produced, The primary output is a csv file containing information about the statistics of
the residual images.

"""
import logging
import sys

import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.time import Time

from rascil.data_models.parameters import rascil_data_path
from rascil.data_models.polarisation import PolarisationFrame
from rascil.data_models.data_model_helpers import export_skycomponent_to_hdf5
from rascil.processing_components import simulate_gaintable_from_voltage_pattern, export_blockvisibility_to_ms
from rascil.processing_components.imaging.primary_beams import create_vp
from rascil.processing_components.simulation.simulation_helpers import find_pb_width_null, create_simulation_components
from rascil.workflows import predict_dft_rsexecute_workflow
from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute
from rascil.workflows.rsexecute.simulation.simulation_rsexecute import \
    create_standard_mid_simulation_rsexecute_workflow

import pprint

pp = pprint.PrettyPrinter()

def init_logging():
    logging.basicConfig(filename='mid_pol_sim.log',
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)

init_logging()
log = logging.getLogger("logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))

def find_vp_actual(telescope="MID_FEKO_B2", normalise=True, test_vp=False):
    vp = create_vp(telescope=telescope)
    if test_vp:
        vp.data[:, 0, ...] = 1.0
        vp.data[:, 1, ...] = 0.0
        vp.data[:, 2, ...] = 0.0
        vp.data[:, 3, ...] = 1.0
    if normalise:
        g = numpy.zeros([4])
        g[0] = numpy.max(numpy.abs(vp.data[:, 0, ...]))
        g[3] = numpy.max(numpy.abs(vp.data[:, 3, ...]))
        g[1] = g[2] = numpy.sqrt(g[0] * g[3])
        for chan in range(4):
            vp.data[:, chan, ...] /= g[chan]
    return vp

def find_vp_renormalised(telescope="MID_FEKO_B2", flatten=True, minpb=0.1):
    vp = create_vp(telescope=telescope)
    g = numpy.zeros([4])
    g[0] = numpy.max(numpy.abs(vp.data[:, 0, ...]))
    g[3] = numpy.max(numpy.abs(vp.data[:, 3, ...]))
    g[1] = g[2] = numpy.sqrt(g[0] * g[3])
    for pol in range(4):
        vp.data[:, pol, ...] /= g[pol]
    if flatten:
        norm = numpy.sqrt(0.5*(numpy.abs(g[0].data[...])**2 + numpy.abs(g[-1].data[...])**2))
        vp.data[norm>minpb] /= norm[norm>minpb]
        vp.data[norm<=minpb] = 0.0
    return vp

    
def do_simulation(args, band='B2',
                  image_polarisation_frame=PolarisationFrame("stokesIQUV"),
                  vis_polarisation_frame=PolarisationFrame("linear")):
    
    pp.pprint(vars(args))
    
    rsexecute.set_client(use_dask=True, n_workers=args.nworkers, threads_per_worker=args.nthreads)
    rsexecute.run(init_logging)
    rsexecute.init_statistics()

    print("\nStarting MID polarisation: simulation\n")

    context = args.context
    offset_dir = args.offset_dir
    pbtype = args.pbtype
    pbradius = args.pbradius
    rmax = args.rmax
    flux_limit = args.flux_limit
    
    # Set up details of simulated observation
    if band == 'B1':
        frequency = [0.765e9]
    elif band == 'B2':
        frequency = [1.36e9]
    elif band == 'Ku':
        frequency = [12.179e9]
    else:
        raise ValueError("Unknown band %s" % band)
    frequency = numpy.array(frequency)

    phasecentre = SkyCoord(ra=args.ra * u.deg, dec=args.declination * u.deg, frame='icrs', equinox='J2000')

    bvis_list = create_standard_mid_simulation_rsexecute_workflow(band, rmax, phasecentre, args.time_range,
                                                                  args.time_chunk,
                                                                  args.integration_time,
                                                                  polarisation_frame=vis_polarisation_frame)
    bvis_list = rsexecute.persist(bvis_list)
    
    # We need the HWHM of the primary beam, and the location of the nulls
    HWHM_deg, null_az_deg, null_el_deg = find_pb_width_null(pbtype, frequency)
    HWHM = HWHM_deg * numpy.pi / 180.0
    FOV_deg = 8.0 * 1.36e9 / frequency[0]
    pb_npixel = 1024
    d2r = numpy.pi / 180.0
    pb_cellsize = d2r * FOV_deg / pb_npixel
    
    # Now construct the components
    original_components, offset_direction = create_simulation_components(context, phasecentre, frequency,
                                                                         pbtype, offset_dir, flux_limit,
                                                                         pbradius * HWHM, pb_npixel, pb_cellsize,
                                                                         polarisation_frame=image_polarisation_frame,
                                                                         filter_by_primary_beam=True)
    
    compfile = args.output_msname.replace(".ms", "_components.hdf5")
    export_skycomponent_to_hdf5(original_components, compfile)
    
    log.info("There are {} components".format(len(original_components)))

    # Make one voltage pattern for each blockvis and persist on Dask
    vp_list = [rsexecute.execute(find_vp_actual)() for ibvis, _ in enumerate(bvis_list)]
    vp_list = rsexecute.persist(vp_list)
    
    # Simulate a gaintable for each component, for each blockvis
    gt_list = [rsexecute.execute(simulate_gaintable_from_voltage_pattern, nout=len(original_components))
               (bvis, original_components, vp_list[ibv], use_radec=False)
               for ibv, bvis in enumerate(bvis_list)]
    
    # Now preict the visibility for each blockvis
    bvis_list = predict_dft_rsexecute_workflow(bvis_list, original_components, gt_list=gt_list)
    bvis_list = rsexecute.persist(bvis_list)
    
    def save_ms(ibvis, bvis):
        times = Time(bvis.time / 86400.0, format='mjd', scale='utc')
        log.info("Writing blockVis {}, time range  {} to {}".format(ibvis, times[0].isot, times[-1].isot))
        msname = args.output_msname.replace('.ms', '_{}.ms'.format(ibvis))
        export_blockvisibility_to_ms(msname, [bvis])
        print("Written: ", numpy.max(numpy.abs(bvis.flagged_vis[:,1:,0,...])))
        return bvis

    bvis_list = [rsexecute.execute(save_ms)(ibvis, bvis) for ibvis, bvis in enumerate(bvis_list)]
    bvis_list = rsexecute.compute(bvis_list, sync=True)
    
    assert len(bvis_list) > 0
    
    from rascil.processing_components import plot_pa
    import matplotlib.pyplot as plt
    plt.clf()
    plot_pa(bvis_list)
    plt.show(block=False)
    
    rsexecute.save_statistics('mid_pol_subtract_model')

    rsexecute.close()
    
    return True


def get_args():
    # Get command line inputs
    import argparse
    
    parser = argparse.ArgumentParser(description='Simulate polarisation effects')
    parser.add_argument('--output_msname', type=str, default="mid_simulation_polarisation.ms",
                        help='MeasurementSet to be written')

    parser.add_argument('--context', type=str, default='s3sky', help='s3sky or singlesource or null')
    
    # Observation definition
    parser.add_argument('--ra', type=float, default=+15.0, help='Right ascension (degrees)')
    parser.add_argument('--declination', type=float, default=-45.0, help='Declination (degrees)')
    parser.add_argument('--rmax', type=float, default=1e3, help='Maximum distance of station from centre (m)')
    parser.add_argument('--band', type=str, default='B2', help="Band")
    parser.add_argument('--integration_time', type=float, default=180.0, help='Integration time (s)')
    parser.add_argument('--time_range', type=float, nargs=2, default=[-4.0, 4.0], help='Time range in hours')
    parser.add_argument('--time_chunk', type=float, default=3600.0, help="Time for a chunk (s)")

    parser.add_argument('--npixel', type=int, default=1536, help='Number of pixels in image')
    parser.add_argument('--offset_dir', type=float, nargs=2, default=[0.0, 0.0], help='Multipliers for null offset')
    parser.add_argument('--pbradius', type=float, default=1.5, help='Radius of sources to include (in HWHM)')
    parser.add_argument('--pbtype', type=str, default='MID', help='Primary beam model: MID or MID_GAUSS')
    parser.add_argument('--seed', type=int, default=18051955, help='Random number seed')
    parser.add_argument('--flux_limit', type=float, default=0.03, help='Flux limit (Jy)')
    
    # Control parameters
    parser.add_argument('--use_radec', type=str, default="False", help='Calculate in RADEC (false)?')
    parser.add_argument('--shared_directory', type=str, default=rascil_data_path('configurations'),
                        help='Location of configuration files')
    
    # Dask parameters
    parser.add_argument('--nthreads', type=int, default=4, help='Number of threads')
    parser.add_argument('--memory', type=int, default=8, help='Memory per worker (GB)')
    parser.add_argument('--nworkers', type=int, default=4, help='Number of workers')
    parser.add_argument('--serial', type=str, default='False', help='Use serial processing?')
    
    # Simulation parameters
    
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    
    args = get_args()
    do_simulation(args)
