#!/usr/bin/env python3

"""Initial script to run polarisation simulations."""

import glob
import json
import logging
import sys

from astropy.time import Time, TimeDelta
import matplotlib
matplotlib.use('Agg')
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt
import numpy
import oskar

LOG = logging.getLogger()

class Simulator(oskar.Interferometer):
    """Simulates and images visibilities concurrently.

    Inherits oskar.Interferometer to image each block in the process_block()
    method.
    """

    def __init__(self, imager, precision=None, settings=None):
        """Creates the simulator, storing a handle to the imager.

        Args:
            imager (oskar.Imager):
                Imager to use.
            precision (Optional[str]):
                Either 'double' or 'single' to specify the numerical
                precision of the simulation. Default 'double'.
            settings (Optional[oskar.SettingsTree]):
                Optional settings to use to set up the simulator.
        """
        oskar.Interferometer.__init__(self, precision, settings)
        self._imager = imager
        self._return_images = 0

    def finalise(self):
        """Called automatically by the base class at the end of run()."""
        oskar.Interferometer.finalise(self)
        if not self.coords_only:
            return self._imager.finalise(return_images=self._return_images)

    def process_block(self, block, block_index):
        """Writes the visibility block to any open file(s), and images it.

        Args:
            block (oskar.VisBlock): A handle to the block to be processed.
            block_index (int):      The index of the visibility block.
        """
        if not self.coords_only:
            self.write_block(block, block_index)
        self._imager.update_from_block(self.vis_header(), block)

    # pylint: disable=arguments-differ
    def run(self, return_images=0):
        """Runs the interferometer simulator and imager.

        Args:
            return_images (int): Number of images to return.
        """
        # Save flags for use in finalise().
        self._return_images = return_images

        # Check if imaging with uniform weighting or W-projection.
        need_coords_first = False
        if self._imager.weighting == 'Uniform' or \
                self._imager.algorithm == 'W-projection':
            need_coords_first = True

        # Simulate coordinates first, if required.
        if need_coords_first:
            self.set_coords_only(True)
            oskar.Interferometer.run(self)
            self.set_coords_only(False)

        # Simulate and image the visibilities.
        return oskar.Interferometer.run(self)

    def set_coords_only(self, value):
        """Calls set_coords_only() on interferometer and imager objects."""
        oskar.Interferometer.set_coords_only(self, value)
        self._imager.set_coords_only(value)


def get_start_time(ra0_deg, length_sec):
    """Returns optimal start time for field RA and observation length."""
    t = Time('2000-01-01 00:00:00', scale='utc', location=('116.764d', '0d'))
    dt_hours = 24.0 - t.sidereal_time('apparent').hour + (ra0_deg / 15.0)
    start = t + TimeDelta(dt_hours * 3600.0 - length_sec / 2.0, format='sec')
    return start.value


def make_plots(prefix, ra0, dec0, frequency_hz, sky_pos, results):
    """Plot selected results."""
    ra = numpy.array([])
    dec = numpy.array([])
    stokes_i = numpy.array([])
    stokes_q = numpy.array([])
    stokes_u = numpy.array([])
    stokes_v = numpy.array([])
    num_pos = sky_pos.shape[0]

    # Loop over source positions.
    for s in range(num_pos):
        key = '%s_%.1f_%.1f_%.0f_MHz' % (
            prefix, sky_pos[s, 0], sky_pos[s, 1], frequency_hz * 1e-6)
        if key in results:
            ra = numpy.append(ra, numpy.radians(sky_pos[s, 0]))
            dec = numpy.append(dec, numpy.radians(sky_pos[s, 1]))
            stokes_i = numpy.append(stokes_i, results[key]['i'])
            stokes_q = numpy.append(stokes_q, results[key]['q'])
            stokes_u = numpy.append(stokes_u, results[key]['u'])
            stokes_v = numpy.append(stokes_v, results[key]['v'])

    # Convert to direction cosines for plotting.
    x = numpy.cos(dec) * numpy.sin(ra - ra0)
    y = numpy.cos(numpy.radians(dec0)) * numpy.sin(dec) - \
            numpy.sin(numpy.radians(dec0)) * numpy.cos(dec) * numpy.cos(ra - ra0)

    # Sub-plots, one for each Stokes parameter.
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(221)
    sc = ax.scatter(x, y, c=stokes_i, s=30, cmap='plasma')
    plt.colorbar(sc)
    plt.gca().set_title('Measured Stokes I')
    plt.gca().axis('equal')
    ax = fig.add_subplot(222)
    sc = ax.scatter(x, y, c=stokes_q, s=30, cmap='plasma')
    plt.colorbar(sc)
    plt.gca().set_title('Measured Stokes Q')
    plt.gca().axis('equal')
    ax = fig.add_subplot(223)
    sc = ax.scatter(x, y, c=stokes_u, s=30, cmap='plasma')
    plt.colorbar(sc)
    plt.gca().set_title('Measured Stokes U')
    plt.gca().axis('equal')
    ax = fig.add_subplot(224)
    sc = ax.scatter(x, y, c=stokes_v, s=30, cmap='plasma')
    plt.colorbar(sc)
    plt.gca().set_title('Measured Stokes V')
    plt.gca().axis('equal')

    # Common axis labels.
    ax = fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax.set_xlabel('x direction cosine', labelpad=20)
    ax.set_ylabel('y direction cosine', labelpad=40)
    # Hide ticks for this new axis.
    ax.set_xticks([])
    ax.set_yticks([])

    # Save and close.
    fig.tight_layout()
    plt.savefig('%s.png' % (prefix))
    plt.close('all')


def run_set(prefix, ra0_deg, dec0_deg, base_settings, sky_pos, results):
    """Run a set of simulations."""
    settings = oskar.SettingsTree('oskar_sim_interferometer')
    num_pos = sky_pos.shape[0]
    settings.from_dict(base_settings)
    frequency_hz = float(settings['observation/start_frequency_hz'])
    for s in range(num_pos):
        ra_deg = sky_pos[s, 0]
        dec_deg = sky_pos[s, 1]

        # Check if already done.
        key = '%s_%.1f_%.1f_%.0f_MHz' % (
            prefix, ra_deg, dec_deg, frequency_hz * 1e-6)
        if key in results:
            continue

        # Set up unpolarised single-source sky model.
        sky = oskar.Sky.from_array([ra_deg, dec_deg, 1.0])

        # Set up imager for this source position.
        fov_ref_frequency_hz = 140e6
        fov_ref_deg = 0.05
        fov_deg = fov_ref_deg * (fov_ref_frequency_hz / frequency_hz)
        imager = oskar.Imager()
        imager.set(fov_deg=fov_deg, image_size=256, image_type='Stokes')
        imager.set_direction(ra_deg, dec_deg)
        if abs(ra_deg - 0) < 0.1 and abs(dec_deg - 0) < 0.1:
            imager.output_root = key

        # Run simulation and make images of all four polarisations.
        LOG.info('Running simulation for source at (%.3f, %.3f)',
                 ra_deg, dec_deg)
        sim = Simulator(imager, settings=settings)
        sim.set_sky_model(sky)
        output = sim.run(return_images=4)
        images = output['images']

        # Store image peak values in results.
        results[key] = {
            'i': max(images[0].min(), images[0].max(), key=abs),
            'q': max(images[1].min(), images[1].max(), key=abs),
            'u': max(images[2].min(), images[2].max(), key=abs),
            'v': max(images[3].min(), images[3].max(), key=abs)
        }

        # Update saved result set.
        with open('results.json', 'w') as output_file:
            json.dump(results, output_file, indent=4)

    # Update plots.
    make_plots(prefix, ra0_deg, dec0_deg, frequency_hz, sky_pos, results)


def main():
    """Main function."""
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    LOG.addHandler(handler)
    LOG.setLevel(logging.INFO)

    # Define common settings.
    ra0_deg = 0.0
    dec0_deg = 0.0
    obs_length_sec = 1.0
    base_settings = {
        'simulator': {
            'use_gpus': 'false',
            'max_sources_per_chunk': '2'
        },
        'observation' : {
            'start_frequency_hz': '110e6',
            'start_time_utc': get_start_time(ra0_deg, obs_length_sec),
            'length': str(obs_length_sec),
            'num_time_steps': '1',
            'phase_centre_ra_deg': str(ra0_deg),
            'phase_centre_dec_deg': str(dec0_deg)
        },
        'telescope': {
            'input_directory': '../Data/SKA1-LOW/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies_equator.tm',
            #'input_directory': '../Data/SKA1-LOW/SKA1-LOW_SKO-0000422_Rev3_38m_equator.tm',
            'aperture_array/array_pattern/enable': 'false'
        },
        'interferometer': {
            'max_time_samples_per_block': '4'
        }
    }

    # Generate source grid.
    sky_grid = oskar.Sky.generate_grid(ra0_deg, dec0_deg, 15, 80.0)
    sky_pos = sky_grid.to_array()

    # Load any saved result sets.
    results = {}
    for json_file in glob.glob('*results.json'):
        with open(json_file, 'r') as input_file:
            results.update(json.load(input_file))

    # Run set.
    prefix = 'pol_scan_stokes_i_skala4'
    run_set(prefix, ra0_deg, dec0_deg, base_settings, sky_pos, results)


if __name__ == '__main__':
