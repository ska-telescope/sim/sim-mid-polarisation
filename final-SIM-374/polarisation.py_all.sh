#!/bin/bash
#!
export PYTHONPATH=.:$PYTHONPATH
for time_range in 0.2 4
do
  for dec in +15 0 -45 -80
  do
    python polarisation.py --rmax 1e4 --npixel 512 \
    --show False --declination ${dec} --band B2 --pbtype MID_FEKO_B2  --integration_time 600 \
    --fov 0.05 --npatch 15 --use_natural True --use_agg True --time_chunk 1800 \
    --time_range -${time_range} ${time_range} --name polarisation_simulation_dec${dec}_time${time_range} | tee -a \
    polarisation_simulation_dec${dec}_time${time_range}.log
  done
done
