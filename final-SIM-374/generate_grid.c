void generate_grid(double ra0_rad, double dec0_rad,
        int side_length, double fov_rad)
{
    int i, j;
    const double l_max = sin(0.5 * fov_rad);
    const double sin_dec0 = sin(dec0_rad);
    const double cos_dec0 = cos(dec0_rad);
    for (j = 0; j < side_length; ++j)
    {
        const double m = 2.0 * l_max * j / (side_length - 1) - l_max;
        for (i = 0; i < side_length; ++i)
        {
            const double l = -2.0 * l_max * i / (side_length - 1) + l_max;
​
            /* Get longitude and latitude from tangent plane coords. */
            const double n = sqrt(1.0 - l*l - m*m);
            const double dec = asin(n * sin_dec0 + m * cos_dec0);
            const double ra = ra0_rad + atan2(l, cos_dec0 * n - m * sin_dec0);
​
            /* Store ra and dec... */
        }
    }
}
